import os
import sys
import json
from subprocess import check_output
from subprocess import CalledProcessError
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from collections import OrderedDict

class Command(BaseCommand):
    CODE_BASE = None;
    MANUAL_EXECUTION = False
    help = 'Setup script for CGE application'

    def add_arguments(self, parser):
        parser.add_argument('--code_base', default='development', help='development or production')

    def handle(self, *args, **options):
        self.MANUAL_EXECUTION = True
        self.check_env_variables()
        self.CODE_BASE = options['code_base'];
        if self.CODE_BASE not in ['development', 'production']:
            print('Error: Invalid code_base')
            return

        print "Starting CGE setup"
        print "CODE BASE: " + self.CODE_BASE
        self.load_migrations()
        print "Ending CGE setup"

    def check_env_variables(self):
        if not os.environ['CODE_BASE']:
            print("!!! Your environment is missing 'CODE_BASE' variable !!!")
            sys.exit(0)

        if not os.environ['ENVIRONMENT']:
            print("!!! Your environment is missing 'ENVIRONMENT' variable !!!")
            sys.exit(0)

    def load_migrations(self):
        print("Loading migrations...")
        try:
            COMMAND = 'cd ' + '/var/www/app' + '&& python manage.py migrate'
            check_output(COMMAND, shell=True)
        except CalledProcessError as err:
            print("Error: Failed to run migrations")
