from django.contrib import admin
from .models import ArticleItem

class ArticleItemAdmin(admin.ModelAdmin):
	pass

admin.site.register(ArticleItem, ArticleItemAdmin)
