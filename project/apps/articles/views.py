from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView

from .models import ArticleItem

class ArticleListView(ListView):
    model = ArticleItem
    queryset = ArticleItem.objects.all().order_by('-publish_date')
