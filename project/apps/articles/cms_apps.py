# -*- coding: utf-8 -*-

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class ArticlesApp(CMSApp):
    name = _('Articles')
    urls = ['project.apps.news.articles']
    app_name = 'articles'

#apphook_pool.register(NewsApp)
