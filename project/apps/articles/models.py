
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.db import models


class ArticleItem(models.Model):
    class Meta:
        app_label = 'articles'
        ordering = ['-publish_date', ]


    author = models.CharField(
        u'author',
        blank=False,
        default='',
        help_text=u'Please enter a list of authors for this article',
        max_length=255)

    title = models.CharField(
        u'title',
        blank=False,
        default='',
        help_text=u'Please enter a title for this article',
        max_length=255)

    journal = models.CharField(
        u'journal',
        blank=False,
        default='',
        help_text=u'Please enter the journal in which the article has been published',
        max_length=255)

    publish_date = models.DateTimeField(
        u'Published date',
        help_text=u'Please enter the date in which the article has been published',
        default=timezone.now)
