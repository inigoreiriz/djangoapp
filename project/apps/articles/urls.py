from django.conf.urls import patterns, url

from .views import ArticleListView


urlpatterns = patterns('',

    # List View
    url(r'^$', ArticleListView.as_view(), name="articleitem_list"),

)
