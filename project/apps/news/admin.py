# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import NewsItem

class NewsItemAdmin(admin.ModelAdmin):
    pass

admin.site.register(NewsItem, NewsItemAdmin)
