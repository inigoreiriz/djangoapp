# -*- coding: utf-8 -*-

from collections import Counter

from datetime import date

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone

from filer.fields.image import FilerImageField
from tinymce import models as tinymce_models

class NewsItem(models.Model):
    class Meta:
        app_label = 'news'
        ordering = ['-news_date', ]

    taints_cache = True

    headline = models.CharField(
        'headline',
        blank=False,
        default='',
        help_text=u'Please provide a unique headline for this news item.',
        max_length=255,
    )

    slug = models.SlugField(
        'slug',
        blank=False,
        default='',
        help_text=u'Please ensure there is a unique “slug” for this news '
                  u'item. Note, it is strongly recommended that this does '
                  u'not change after publishing this news item.',
        max_length=255,
    )

    news_date = models.DateTimeField(
        u'news date',
        blank=False,
        default=timezone.now,
        help_text=u'Please provide the date of this news item. Note, setting '
                  u'this to a future date will prevent this news item from '
                  u'appearing until that time.'
    )

    mod_date = models.DateTimeField(
        u'modification date',
        auto_now=True,
        editable=False,
    )

    news_image = FilerImageField(
        blank=True,
        help_text=u'Optional. Please supply an image, if one is desired. This '
                  u'will be resized automatically.',
        null=True,
        related_name='news_key_image',
    )

    sub_headline = tinymce_models.HTMLField()

    news_description = tinymce_models.HTMLField()

    link = models.URLField(
        u'link',
        blank=True,
        default='',
        help_text=u'Optional. Please provide an external link for this news item.',

    )

    def get_absolute_url(self):
        return reverse('news_item_detail', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.headline
