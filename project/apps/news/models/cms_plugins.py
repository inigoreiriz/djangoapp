# -*- coding: utf-8 -*-

from django.db import models

from cms.models import CMSPlugin

class RecentNewsPluginModel(CMSPlugin):
    class Meta:
        app_label = 'news'

    taints_cache = True

    max_items = models.PositiveIntegerField('max. number',
        blank=False,
        default=5,
        help_text=u'Please provide the maximum number of items to display (0 means unlimited)',
    )
