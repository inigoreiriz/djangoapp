# -*- coding: utf-8 -*-

from django.conf import settings
from django.db.models import Count
from django.utils import timezone

from cms.models import CMSPlugin
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from .models import (
    NewsItem,
    RecentNewsPluginModel,
)

class RecentNewsPlugin(CMSPluginBase):
    model = RecentNewsPluginModel
    name = u'Recent News'
    render_template = "news/_recent_news_plugin.html"
    text_enabled = True
    cache = False

    def render(self, context, instance, placeholder):
        request = context.get('request', None)
        toolbar = getattr(request, 'toolbar', False)

        if toolbar and toolbar.edit_mode:
            news_items = NewsItem.objects.order_by('-news_date')
        else:
            news_items = NewsItem.objects.filter(news_date__lte=timezone.now).order_by('-news_date')

        if instance.max_items:
            news_items = news_items[:instance.max_items]

        context['news_items'] = news_items
        context['instance'] = instance
        return context        

plugin_pool.register_plugin(RecentNewsPlugin)
