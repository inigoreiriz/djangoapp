# -*- coding: utf-8 -*-

from datetime import date

from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView
from django.views.generic.base import RedirectView
from django.views.generic.dates import ArchiveIndexView
from django.views.generic import TemplateView

from .models import NewsItem


class NewsItemDetailView(DetailView):
    model = NewsItem

    def get_queryset(self):
        qs = super(NewsItemDetailView, self).get_queryset()

        return qs.filter(news_date__lte=timezone.now)


    def get_context_data(self, **kwargs):

        context = super(NewsItemDetailView, self).get_context_data(**kwargs)
        return context

class NewsItemListView(ListView):
    model = NewsItem
