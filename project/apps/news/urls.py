# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from .views import (
    NewsItemDetailView,
    NewsItemListView,
)

urlpatterns = patterns('',
    # News Item List
    url(r'^$', NewsItemListView.as_view(), name='news_item_list'),
    # News Item Detail
    url(r'^(?P<slug>[^/]+)/$', NewsItemDetailView.as_view(), name='news_item_detail'),
)
