from django.core.urlresolvers import reverse
from django.db import models

from tinymce import models as tinymce_models
from filer.fields.image import FilerImageField

class StaffMember(models.Model):
    class Meta:
        app_label = 'staff'

    full_name = models.CharField(
        u'full name',
        blank=False,
        default='',
        help_text=u'Please enter a full name for this staff member',
        max_length=64,
        unique=True,
    )

    photo = FilerImageField(
        blank=True,
        help_text=u'Optional. Please supply a photo of this staff member.',
        null=True,
        on_delete=models.SET_NULL,  # Important
    )

    bio = tinymce_models.HTMLField()

    url = models.URLField(
        blank = True,
        help_text= u'Optional. Please provide a valid URL for this staff member.',
    )

    def __unicode__(self):
        return self.full_name
