from django.contrib import admin
from .models import StaffMember

class StaffMemberAdmin(admin.ModelAdmin):
	pass

admin.site.register(StaffMember, StaffMemberAdmin)
