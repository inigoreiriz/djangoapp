
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView

from .models import StaffMember

class StaffListView(ListView):
    model = StaffMember
    queryset = StaffMember.objects.all()
