FROM library/ubuntu:xenial

MAINTAINER inigoreiriz@gmail.com

# Creating user
RUN useradd developer
RUN echo "developer:docker" | chpasswd

# Set environment variables
ENV TERM=xterm
ENV DEBUG='true'
ENV CODE_BASE=development
ENV ENVIRONMENT=docker

# Add favourite commands
RUN echo 'alias ..="cd .."' > /root/.bash_aliases
RUN echo 'alias ll="ls -alF"' >> /root/.bash_aliases
RUN echo 'alias pyc="find $(pwd) -type f -name *.pyc -delete"' >> /root/.bash_aliases
RUN echo 'alias pyo="find $(pwd) -type f -name *.pyo -delete"' >> /root/.bash_aliases

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    git \
    mc \
    nginx \
    python-pip \
    python-dev \
    libtiff5-dev \
    libjpeg8-dev \
    zlib1g-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libwebp-dev \
    tcl8.6-dev \
    tig \
    tk8.6-dev \
    python-tk \
    libpq-dev \
    python-psycopg2 \
 	  python2.7 \
    vim \
    supervisor

RUN pip install --upgrade pip
RUN pip install uwsgi
RUN pip install gunicorn

RUN mkdir /var/www/app
WORKDIR /var/www/app
