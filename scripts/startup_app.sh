#!/bin/bash
if [ "$ENVIRONMENT" = "docker" ]; then

  # UPDATE PIP
  pip install --upgrade pip

  # DJANGO REQUIREMENTS
  pip install -r requirements.txt

  if [ "$CODE_BASE" = "development" ]; then

    #NGINX Config
    ln -s /var/www/app/project/config/services/app.nginx.development.conf /etc/nginx/sites-available/app.nginx.development.conf
    ln -s /etc/nginx/sites-available/app.nginx.development.conf /etc/nginx/sites-enabled/app.nginx.development.conf

    python manage.py app_setup --code_base development

  fi

  #START SERVICES
  echo Starting nginx..
  /usr/sbin/nginx

  echo Starting gunicorn..
  exec gunicorn project.config.wsgi -c /var/www/app/project/config/services/app.gunicorn.conf.py

fi
